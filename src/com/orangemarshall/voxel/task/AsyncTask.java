package com.orangemarshall.voxel.task;

import com.orangemarshall.voxel.Voxel;
import org.bukkit.Bukkit;


public abstract class AsyncTask implements Runnable{

    protected Voxel plugin;

    public AsyncTask(Voxel plugin){
        this.plugin = plugin;
    }

    @Override
    public void run(){
        doWorkAsync();

        Bukkit.getScheduler().callSyncMethod(plugin, () -> {
            finishWorkSync();

            plugin.getTaskManager().finishedTask();

            if(this instanceof AsyncToolTask){
                AsyncToolTask task = (AsyncToolTask) this;
                task.voxelPlayer.getHistoryList().add(task.getRevertTask());
            }

            return null;
        });
    }

    public void prepareWorkSync(){}
    public abstract void doWorkAsync();
    public void finishWorkSync(){}

}

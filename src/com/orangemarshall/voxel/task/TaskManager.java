package com.orangemarshall.voxel.task;

import com.google.common.collect.Lists;
import com.orangemarshall.voxel.Voxel;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.Queue;


public class TaskManager{

    private Queue<AsyncTask> queuedTasks = Lists.newLinkedList();
    private Thread executingTask = null;
    private Voxel plugin;

    public TaskManager(Voxel plugin){
        this.plugin = plugin;
        new ExecuteTask().runTaskTimer(plugin, 5, 5);
    }

    public void offerTask(AsyncTask task){
        this.queuedTasks.offer(task);
    }

    public void finishedTask(){
        executingTask = null;
    }

    private boolean isBusy(){
        return executingTask != null && executingTask.isAlive();
    }


    private class ExecuteTask extends BukkitRunnable{

        @Override
        public void run(){
            if(!isBusy() && !queuedTasks.isEmpty()){
                AsyncTask task = queuedTasks.poll();

                executingTask = new Thread(task);
                executingTask.setDaemon(true);

                task.prepareWorkSync();
                executingTask.start();
            }
        }
    }
}

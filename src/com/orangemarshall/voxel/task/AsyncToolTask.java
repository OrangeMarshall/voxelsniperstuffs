package com.orangemarshall.voxel.task;

import com.orangemarshall.voxel.util.BlockType;
import com.orangemarshall.voxel.Voxel;
import com.orangemarshall.voxel.VoxelPlayer;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;


public abstract class AsyncToolTask extends AsyncTask{

    protected VoxelPlayer voxelPlayer;
    protected Location selectedBlock;
    protected World world;

    protected int size;
    protected BlockType selectedType;

    public AsyncToolTask(Voxel plugin, VoxelPlayer voxelPlayer, Block selectedBlock){
        super(plugin);
        this.voxelPlayer = voxelPlayer;
        this.world = selectedBlock.getWorld();
        this.selectedBlock = selectedBlock.getLocation();
        this.size = voxelPlayer.getSelectedToolSize();
        this.selectedType = voxelPlayer.getSelectedType();
    }

    public abstract AsyncTask getRevertTask();

}

package com.orangemarshall.voxel;

import com.orangemarshall.voxel.gui.VoxelMainMenu;
import com.orangemarshall.voxel.util.gui.GuiHolder;
import com.orangemarshall.voxel.util.gui.GuiScreen;
import com.orangemarshall.voxel.task.AsyncTask;
import com.orangemarshall.voxel.util.HistoryList;
import com.orangemarshall.voxel.util.Util;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Optional;

import static org.bukkit.ChatColor.*;


public class VoxelCommands implements CommandExecutor{

    private Voxel plugin;

    public VoxelCommands(Voxel plugin){
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String label, String[] args){
        if(!(commandSender instanceof Player)){
            return false;
        }

        Player player = (Player) commandSender;


        if(label.equalsIgnoreCase("vmaterial")){
            if(args.length == 2){
                handleSetMaterial(player, args[0], args[1]);
                return true;
            }else if(args.length == 1){
                handleSetMaterial(player, args[0], "0");
                return true;
            }

        }else if(label.equalsIgnoreCase("vtool") && args.length == 1){
            handleSetTool(player, args[0]);
            return true;

        }else if(label.equalsIgnoreCase("vundo") && args.length == 0){
            handleRevertTask(player);
            return true;

        }else if(label.equalsIgnoreCase("vsize") && args.length == 1){
            handleSetSize(player, args[0]);
            return true;

        }else if((label.equalsIgnoreCase("vgui") || label.equalsIgnoreCase("vhelp")) && args.length == 0){
            handleOpenGui(player);
            return true;

        }

        return false;
    }

    private void handleSetMaterial(Player player, String materialArg, String dataArg){
        try{
            byte data = Byte.parseByte(dataArg);
            Material material = Material.matchMaterial(materialArg);

            if(material == null){
                Util.sendErrorMessage(player, "Could not find Material matching " + YELLOW + materialArg);
                return;
            }

            VoxelPlayer.getOrCreatePlayer(player).setSelectedType(material, data);

            Util.sendInfoMessage(player, "Selected " + GREEN + material.toString() + YELLOW + " (" + AQUA + data + YELLOW + ")");
        }catch(NumberFormatException e){
            Util.sendErrorMessage(player, "Invalid data: Could not convert data to byte");
        }
    }

    private void handleSetTool(Player player, String toolArg){
        Optional<Voxel.Tool> optionalTool = Voxel.Tool.of(toolArg);

        if(optionalTool.isPresent()){
            Voxel.Tool tool = optionalTool.get();

            VoxelPlayer.getOrCreatePlayer(player).setSelectedTool(tool);
            Util.sendInfoMessage(player, "Selected " + GOLD + tool.getName());
        }else{
            Util.sendErrorMessage(player, "Could not find Tool matching " + YELLOW + toolArg);
        }
    }

    private void handleSetSize(Player player, String toolArg){
        try{
            int size = Integer.parseUnsignedInt(toolArg);

            if(size == 0){
                throw new NumberFormatException();
            }

            VoxelPlayer.getOrCreatePlayer(player).setSelectedToolSize(size);
            Util.sendInfoMessage(player, "Set size to " + GRAY + size);
        }catch(NumberFormatException e){
            Util.sendErrorMessage(player, "Could not convert to number");
        }
    }

    private void handleRevertTask(Player player){
        HistoryList<AsyncTask> history = VoxelPlayer.getOrCreatePlayer(player).getHistoryList();
        AsyncTask lastTask = history.getMostRecent();

        if(lastTask != null){
            plugin.getTaskManager().offerTask(lastTask);
            history.removeMostRecent();
        }else{
            Util.sendInfoMessage(player, "No actions to revert");
        }
    }

    private void handleOpenGui(Player player){
        GuiHolder holder = new GuiHolder(plugin);
        GuiScreen screen = new VoxelMainMenu(holder, player);

        holder.openGui(screen, player);
    }

}

package com.orangemarshall.voxel.listener;

import com.orangemarshall.voxel.task.AsyncTask;
import com.orangemarshall.voxel.event.VoxelTaskEvent;
import com.orangemarshall.voxel.Voxel;
import com.orangemarshall.voxel.VoxelPlayer;
import com.orangemarshall.voxel.tool.VoxelProvider;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;


public class VoxelListener implements Listener{

    private Voxel plugin;

    public VoxelListener(Voxel plugin){
        this.plugin = plugin;
    }

    @EventHandler
    public void onVoxelTask(VoxelTaskEvent event){
        VoxelPlayer voxelPlayer = event.voxelPlayer;
        VoxelProvider provider = voxelPlayer.getSelectedTool().getProvider();

        AsyncTask task = provider.getAsyncTask(voxelPlayer, event.selectedBlock);
        plugin.getTaskManager().offerTask(task);
    }

}

package com.orangemarshall.voxel.listener;

import com.orangemarshall.voxel.event.VoxelTaskEvent;
import com.orangemarshall.voxel.util.Util;
import com.orangemarshall.voxel.Voxel;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;


public class DispatchListener implements Listener{

    private Voxel plugin;

    public DispatchListener(Voxel plugin){
        this.plugin = plugin;
    }

    @EventHandler
    public void onInteract(PlayerInteractEvent event){
        if(event.getAction() == Action.RIGHT_CLICK_AIR || event.getAction() == Action.RIGHT_CLICK_BLOCK){
            ItemStack itemStack = event.getPlayer().getItemInHand();

            if(itemStack != null && itemStack.getType() == Material.BROWN_MUSHROOM){
                Player player = event.getPlayer();

                Block blockAimedAt = event.getClickedBlock() == null ? Util.getBlockLookedAt(player) : event.getClickedBlock();

                if(blockAimedAt != null){
                    callEvent(new VoxelTaskEvent(player, blockAimedAt));
                    event.setCancelled(true);
                }else{
                    Util.sendInfoMessage(player, "The selected block you selected was too far away!");
                }
            }

        }
    }

    private void callEvent(Event event){
        plugin.getServer().getPluginManager().callEvent(event);
    }

}

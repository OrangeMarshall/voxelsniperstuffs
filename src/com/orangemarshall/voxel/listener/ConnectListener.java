package com.orangemarshall.voxel.listener;

import com.orangemarshall.voxel.VoxelPlayer;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;


public class ConnectListener implements Listener{

    @EventHandler
    public void onConnect(PlayerJoinEvent event){
        VoxelPlayer.getOrCreatePlayer(event.getPlayer());
    }

    @EventHandler
    public void onDisconnect(PlayerQuitEvent event){
        VoxelPlayer.removePlayer(event.getPlayer());
    }

}

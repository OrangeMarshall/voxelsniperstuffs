package com.orangemarshall.voxel.replacer;

import com.orangemarshall.voxel.Voxel;
import com.orangemarshall.voxel.task.AsyncTask;
import com.orangemarshall.voxel.util.WorldSnapshot;
import net.minecraft.server.v1_8_R3.Chunk;
import net.minecraft.server.v1_8_R3.ChunkProviderServer;
import org.bukkit.World;
import org.bukkit.craftbukkit.v1_8_R3.CraftWorld;
import org.bukkit.craftbukkit.v1_8_R3.util.LongHash;

import java.util.Collection;
import java.util.Set;


public class BlockReplacer{

    private Voxel plugin;

    private final WorldSnapshot worldSnapshot;
    private Set<Chunk> backup = null;

    // Replaces chunks created by a WorldSnapshot and replaces them in the actual world.
    public BlockReplacer(Voxel plugin, World world, Collection<org.bukkit.Chunk> chunks){
        worldSnapshot = new WorldSnapshot(world, chunks);
        this.plugin = plugin;
    }

    public void backupCurrentState(){
        backup = worldSnapshot.createChunks();
    }

    public WorldSnapshot getWorldSnapshot(){
        return worldSnapshot;
    }

    public void doReplace(){
        Set<Chunk> nms_chunks = worldSnapshot.createChunks();
        replaceChunks(nms_chunks);
    }

    private void replaceChunks(Collection<Chunk> nms_chunks){
        for(Chunk nms_chunk : nms_chunks){
            CraftWorld craftWorld = nms_chunk.getWorld().getWorld();
            ChunkProviderServer chunkProviderServer = craftWorld.getHandle().chunkProviderServer;

            int x = nms_chunk.locX;
            int z = nms_chunk.locZ;

            chunkProviderServer.unloadQueue.remove(x, z);
            chunkProviderServer.chunks.put(LongHash.toLong(x, z), nms_chunk);

            craftWorld.unloadChunk(x, z);
            craftWorld.loadChunk(x, z);
            craftWorld.refreshChunk(x, z);
        }
    }

    public AsyncTask getRevertTask(){
        if(backup == null){
            throw new IllegalStateException("No backup has been created yet");
        }

        return new RevertTask(plugin, backup);
    }


    public class RevertTask extends AsyncTask{

        private Collection<Chunk> oldChunks;

        public RevertTask(Voxel plugin, Collection<Chunk> oldChunks){
            super(plugin);
            this.oldChunks = oldChunks;
        }

        @Override
        public void prepareWorkSync(){
            replaceChunks(oldChunks);
        }

        @Override
        public void doWorkAsync(){}

    }
}

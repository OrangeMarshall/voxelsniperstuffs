package com.orangemarshall.voxel.tool;

import com.google.common.collect.Lists;
import com.orangemarshall.voxel.Voxel;
import com.orangemarshall.voxel.VoxelPlayer;
import com.orangemarshall.voxel.replacer.BlockReplacer;
import com.orangemarshall.voxel.task.AsyncTask;
import com.orangemarshall.voxel.task.AsyncToolTask;
import org.bukkit.Chunk;
import org.bukkit.block.Block;


public class RegenerateChunkProvider implements VoxelProvider{

    private Voxel plugin;

    public RegenerateChunkProvider(Voxel plugin){
        this.plugin = plugin;
    }

    @Override
    public AsyncTask getAsyncTask(VoxelPlayer voxelPlayer, Block selectedBlock){
        return new RegenerateChunkTask(plugin, voxelPlayer, selectedBlock);
    }

    private static class RegenerateChunkTask extends AsyncToolTask{

        private BlockReplacer replacer;

        public RegenerateChunkTask(Voxel plugin, VoxelPlayer voxelPlayer, Block selectedBlock){
            super(plugin, voxelPlayer, selectedBlock);
        }

        @Override
        public void doWorkAsync(){
        }

        @Override
        public void finishWorkSync(){
            Chunk chunk = selectedBlock.getChunk();
            int x = chunk.getX();
            int z = chunk.getZ();

            replacer = new BlockReplacer(plugin, world, Lists.newArrayList(chunk));
            replacer.backupCurrentState();

            chunk.getWorld().regenerateChunk(x, z);
            chunk.getWorld().refreshChunk(x, z);
        }

        @Override
        public AsyncTask getRevertTask(){
            return replacer.getRevertTask();
        }
    }

}

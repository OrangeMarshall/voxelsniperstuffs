package com.orangemarshall.voxel.tool;

import com.google.common.collect.Sets;
import com.orangemarshall.voxel.util.MathHelper;
import com.orangemarshall.voxel.Voxel;
import com.orangemarshall.voxel.VoxelPlayer;
import com.orangemarshall.voxel.util.WorldSnapshot;
import com.orangemarshall.voxel.replacer.BlockReplacer;
import com.orangemarshall.voxel.task.AsyncTask;
import com.orangemarshall.voxel.task.AsyncToolTask;
import org.bukkit.Chunk;
import org.bukkit.Material;
import org.bukkit.block.Block;

import java.util.Set;


public class OverlayBrushProvider  implements VoxelProvider{

    private Voxel plugin;

    public OverlayBrushProvider(Voxel plugin){
        this.plugin = plugin;
    }

    @Override
    public AsyncTask getAsyncTask(VoxelPlayer voxelPlayer, Block selectedBlock){
        return new OverlayBrushTask(plugin, voxelPlayer, selectedBlock);
    }

    private static class OverlayBrushTask extends AsyncToolTask{

        private BlockReplacer replacer;

        private int posX, posY, posZ;
        private int minX, maxX, minZ, maxZ;

        public OverlayBrushTask(Voxel plugin, VoxelPlayer voxelPlayer, Block selectedBlock){
            super(plugin, voxelPlayer, selectedBlock);
        }

        @Override
        public void prepareWorkSync(){
            Set<Chunk> chunks = Sets.newHashSet();

            posX = selectedBlock.getBlockX();
            posY = selectedBlock.getBlockY();
            posZ = selectedBlock.getBlockZ();

            minX = posX - size;
            maxX = posX + size + 1;
            minZ = posZ- size;
            maxZ = posZ + size + 1;

            for(int x = minX; x <= maxX; x++){
                for(int z = minZ; z <= maxZ; z++){
                    chunks.add(world.getBlockAt(x, 0, z).getChunk());
                }
            }

            replacer = new BlockReplacer(plugin, world, chunks);
        }

        @Override
        public void doWorkAsync(){
            replacer.backupCurrentState();

            int size = voxelPlayer.getSelectedToolSize();
            int sizeSquared = MathHelper.pow(size, 2);

            WorldSnapshot snapshot = replacer.getWorldSnapshot();

            for (int x = minX; x <= maxX; x++) {
                double xSquared = (posX - x) * (posX - x);
                for (int z = minZ; z <= maxZ; z++) {
                    double zSquared = (posZ - z) * (posZ - z);
                    if (xSquared + zSquared < sizeSquared) {
                        int y = posY;
                        for (; y >= 0; y--) {
                            if (snapshot.getTypeAt(x, y, z).material != Material.AIR) {
                                break;
                            }
                        }
                        if (y == posY && y < 256) {
                            if (snapshot.getTypeAt(x, y + 1, z).material != Material.AIR) {
                                continue;
                            }
                        }

                        int depth = 3;
                        for (int y0 = y; y0 > y - depth; y0--) {
                            if (snapshot.getTypeAt(x, y0, z).material != Material.AIR) {
                                snapshot.setTypeAt(x, y, z, selectedType);
                            }
                        }
                    }
                }
            }
        }

        @Override
        public void finishWorkSync(){
            replacer.doReplace();
        }

        @Override
        public AsyncTask getRevertTask(){
            return replacer.getRevertTask();
        }
    }

}
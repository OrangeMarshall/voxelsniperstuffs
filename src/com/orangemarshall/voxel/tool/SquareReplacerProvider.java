package com.orangemarshall.voxel.tool;

import com.google.common.collect.Sets;
import com.orangemarshall.voxel.Voxel;
import com.orangemarshall.voxel.VoxelPlayer;
import com.orangemarshall.voxel.replacer.BlockReplacer;
import com.orangemarshall.voxel.task.AsyncTask;
import com.orangemarshall.voxel.task.AsyncToolTask;
import com.orangemarshall.voxel.util.WorldSnapshot;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.block.Block;

import java.util.Set;


public class SquareReplacerProvider implements VoxelProvider{

    private Voxel plugin;

    public SquareReplacerProvider(Voxel plugin){
        this.plugin = plugin;
    }

    @Override
    public AsyncTask getAsyncTask(VoxelPlayer voxelPlayer, Block selectedBlock){
        return new SquareReplacerTask(plugin, voxelPlayer, selectedBlock);
    }

    public static class SquareReplacerTask extends AsyncToolTask{

        private BlockReplacer replacer;

        public SquareReplacerTask(Voxel plugin, VoxelPlayer voxelPlayer, Block selectedBlock){
            super(plugin, voxelPlayer, selectedBlock);
        }

        @Override
        public void prepareWorkSync(){
            Set<Chunk> chunks = Sets.newHashSet();

            int halfSize = size >> 1;

            for(int x = -halfSize; x <= halfSize; x++){
                for(int z = -halfSize; z <= halfSize; z++){
                    chunks.add(selectedBlock.clone().add(x, 0, z).getChunk());
                }
            }

            replacer = new BlockReplacer(plugin, world, chunks);
        }

        @Override
        public void doWorkAsync(){
            replacer.backupCurrentState();

            WorldSnapshot worldSnapshot = replacer.getWorldSnapshot();
            int halfSize = size >> 1;

            for(int x = -halfSize; x <= halfSize; x++){
                for(int z = -halfSize; z <= halfSize; z++){
                    Location modify = selectedBlock.clone().add(x, 0, z);
                    worldSnapshot.setTypeAt(modify.getBlockX(), modify.getBlockY(), modify.getBlockZ(), selectedType);
                }
            }
        }

        @Override
        public void finishWorkSync(){
            replacer.doReplace();
        }

        @Override
        public AsyncTask getRevertTask(){
            return replacer.getRevertTask();
        }
    }

}

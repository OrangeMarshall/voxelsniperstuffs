package com.orangemarshall.voxel.tool;

import com.orangemarshall.voxel.VoxelPlayer;
import com.orangemarshall.voxel.task.AsyncTask;
import org.bukkit.block.Block;


public interface VoxelProvider{

    AsyncTask getAsyncTask(VoxelPlayer voxelPlayer, Block selectedBlock);

}

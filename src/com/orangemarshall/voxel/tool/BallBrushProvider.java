package com.orangemarshall.voxel.tool;

import com.google.common.collect.Sets;
import com.orangemarshall.voxel.Voxel;
import com.orangemarshall.voxel.VoxelPlayer;
import com.orangemarshall.voxel.replacer.BlockReplacer;
import com.orangemarshall.voxel.task.AsyncTask;
import com.orangemarshall.voxel.task.AsyncToolTask;
import com.orangemarshall.voxel.util.MathHelper;
import com.orangemarshall.voxel.util.WorldSnapshot;
import org.bukkit.Chunk;
import org.bukkit.block.Block;

import java.util.Set;


public class BallBrushProvider implements VoxelProvider{

    private Voxel plugin;

    public BallBrushProvider(Voxel plugin){
        this.plugin = plugin;
    }

    @Override
    public AsyncTask getAsyncTask(VoxelPlayer voxelPlayer, Block selectedBlock){
        return new BallBrushTask(plugin, voxelPlayer, selectedBlock);
    }

    private static class BallBrushTask extends AsyncToolTask{

        private BlockReplacer replacer;
        private int posX, posY, posZ;
        private int sizeSquared;

        public BallBrushTask(Voxel plugin, VoxelPlayer voxelPlayer, Block selectedBlock){
            super(plugin, voxelPlayer, selectedBlock);
        }

        @Override
        public void prepareWorkSync(){
            sizeSquared = MathHelper.pow(size, 2);

            posX = selectedBlock.getBlockX();
            posY = selectedBlock.getBlockY();
            posZ = selectedBlock.getBlockZ();

            Set<Chunk> chunks = Sets.newHashSet();

            for (int z = -size; z <= size; z++) {
                for (int x = -size; x <= size; x++){
                    chunks.add(world.getBlockAt(posX + x, 0, posZ + z).getChunk());
                }
            }

            replacer = new BlockReplacer(plugin, world, chunks);
        }

        @Override
        public void doWorkAsync(){
            replacer.backupCurrentState();
            WorldSnapshot snapshot = replacer.getWorldSnapshot();

            for (int z = 1; z <= size; z++) {
                int zSquared = MathHelper.pow(z, 2);

                setWithClampedY(snapshot, posX + z, posY, posZ);
                setWithClampedY(snapshot, posX - z, posY, posZ);
                setWithClampedY(snapshot, posX, posY + z, posZ);
                setWithClampedY(snapshot, posX, posY - z, posZ);
                setWithClampedY(snapshot, posX, posY, posZ + z);
                setWithClampedY(snapshot, posX, posY, posZ - z);

                for (int x = 1; x <= size; x++) {
                    int xSquared = MathHelper.pow(x, 2);

                    if (zSquared + xSquared <= sizeSquared) {
                        setWithClampedY(snapshot, posX + z, posY, posZ + x);
                        setWithClampedY(snapshot, posX + z, posY, posZ - x);
                        setWithClampedY(snapshot, posX - z, posY, posZ + x);
                        setWithClampedY(snapshot, posX - z, posY, posZ - x);
                        setWithClampedY(snapshot, posX + z, posY + x, posZ);
                        setWithClampedY(snapshot, posX + z, posY - x, posZ);
                        setWithClampedY(snapshot, posX - z, posY + x, posZ);
                        setWithClampedY(snapshot, posX - z, posY - x, posZ);
                        setWithClampedY(snapshot, posX, posY + z, posZ + x);
                        setWithClampedY(snapshot, posX, posY + z, posZ - x);
                        setWithClampedY(snapshot, posX, posY - z, posZ + x);
                        setWithClampedY(snapshot, posX, posY - z, posZ - x);
                    }

                    for (int y = 1; y <= size; y++) {
                        if (xSquared + MathHelper.pow(y, 2) + zSquared <= sizeSquared) {
                            setWithClampedY(snapshot, posX + x, posY + y, posZ + z);
                            setWithClampedY(snapshot, posX + x, posY + y, posZ - z);
                            setWithClampedY(snapshot, posX - x, posY + y, posZ + z);
                            setWithClampedY(snapshot, posX - x, posY + y, posZ - z);
                            setWithClampedY(snapshot, posX + x, posY - y, posZ + z);
                            setWithClampedY(snapshot, posX + x, posY - y, posZ - z);
                            setWithClampedY(snapshot, posX - x, posY - y, posZ + z);
                            setWithClampedY(snapshot, posX - x, posY - y, posZ - z);
                        }
                    }
                }
            }
        }

        @Override
        public void finishWorkSync(){
            replacer.doReplace();
        }

        private void setWithClampedY(WorldSnapshot snapshot, int x, int y, int z){
            snapshot.setTypeAt(x, clampY(y), z, selectedType);
        }

        private int clampY(int y){
            y = Math.max(y, 0);
            y = Math.min(y, 255);
            return y;
        }

        @Override
        public AsyncTask getRevertTask(){
            return replacer.getRevertTask();
        }
    }

}

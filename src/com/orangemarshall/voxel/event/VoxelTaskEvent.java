package com.orangemarshall.voxel.event;

import com.orangemarshall.voxel.VoxelPlayer;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;


public class VoxelTaskEvent extends Event{

    private static final HandlerList handlers = new HandlerList();

    public final VoxelPlayer voxelPlayer;
    public final Block selectedBlock;

    public VoxelTaskEvent(Player player, Block selectedBlock){
        this.voxelPlayer = VoxelPlayer.getOrCreatePlayer(player);
        this.selectedBlock = selectedBlock;
    }

    public static HandlerList getHandlerList(){
        return handlers;
    }

    @Override
    public HandlerList getHandlers(){
        return handlers;
    }
}

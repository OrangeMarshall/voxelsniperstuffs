package com.orangemarshall.voxel;

import com.google.common.collect.Lists;
import com.orangemarshall.voxel.listener.ConnectListener;
import com.orangemarshall.voxel.listener.DispatchListener;
import com.orangemarshall.voxel.listener.VoxelListener;
import com.orangemarshall.voxel.task.TaskManager;
import com.orangemarshall.voxel.tool.*;
import com.orangemarshall.voxel.util.ActionbarRepeater;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.List;
import java.util.Optional;


public class Voxel extends JavaPlugin{

    //TODO permissions
    //TODO smart chunk choosing / improve tools' work
    //TODO entity issues

    private static Voxel instance;

    private DispatchListener dispatchListener;
    private VoxelListener voxelListener;
    private ActionbarRepeater actionbarRepeater;

    private VoxelCommands commandExec;

    private ConnectListener connectListener;

    private TaskManager taskManager;

    public Voxel(){
        instance = this;
    }

    public TaskManager getTaskManager(){
        return this.taskManager;
    }

    @Override
    public void onEnable(){
        VoxelPlayer.registerOnlinePlayers();

        dispatchListener = new DispatchListener(this);
        voxelListener = new VoxelListener(this);
        connectListener = new ConnectListener();

        taskManager = new TaskManager(this);

        actionbarRepeater = new ActionbarRepeater(this);
        actionbarRepeater.restart();

        getServer().getPluginManager().registerEvents(dispatchListener, this);
        getServer().getPluginManager().registerEvents(voxelListener, this);
        getServer().getPluginManager().registerEvents(connectListener, this);

        commandExec = new VoxelCommands(this);
        getCommand("vmaterial").setExecutor(commandExec);
        getCommand("vtool").setExecutor(commandExec);
        getCommand("vundo").setExecutor(commandExec);
        getCommand("vsize").setExecutor(commandExec);
        getCommand("vgui").setExecutor(commandExec);
        getCommand("vhelp").setExecutor(commandExec);
    }



    public enum Tool{
        REPLACE_CUBE("SquareReplacer", new SquareReplacerProvider(instance)),
        BALL_BRUSH("BallBrush", new BallBrushProvider(instance), "ball"),
        REGEN_CHUNK("RegenerateChunk", new RegenerateChunkProvider(instance), "regen", "regenerate", "regeneratechunk", "regenchunk"),
        OVERLAY_BRUSH("OverlayBrush", new OverlayBrushProvider(instance), "overlay");

        private String name;
        private List<String> aliases;
        private VoxelProvider provider;

        Tool(String name, VoxelProvider provider, String... aliases){
            this.name = name;
            this.provider = provider;
            this.aliases = Lists.newArrayList(aliases);
        }

        public VoxelProvider getProvider(){
            return provider;
        }

        public String getName(){
            return name;
        }

        public List<String> getAliases(){
            return aliases;
        }

        public static Optional<Tool> of(String name){
            for(Tool tool : values()){
                List<String> aliases = tool.getAliases();
                aliases.add(tool.getName());
                aliases.add(tool.name());

                boolean isRequestedTool = aliases.stream().filter(alias -> alias.equalsIgnoreCase(name)).count() > 0;

                if(isRequestedTool){
                    return Optional.of(tool);
                }
            }

            return Optional.empty();
        }

    }

}

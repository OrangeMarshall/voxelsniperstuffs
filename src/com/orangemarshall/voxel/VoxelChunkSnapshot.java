package com.orangemarshall.voxel;

import net.minecraft.server.v1_8_R3.BiomeBase;
import net.minecraft.server.v1_8_R3.ChunkSection;
import net.minecraft.server.v1_8_R3.IBlockData;
import org.bukkit.ChunkSnapshot;
import org.bukkit.block.Biome;
import org.bukkit.craftbukkit.v1_8_R3.CraftChunk;
import org.bukkit.craftbukkit.v1_8_R3.block.CraftBlock;

import java.util.Arrays;


/*
    Based on CraftChunkSnapshot
*/
public class VoxelChunkSnapshot implements ChunkSnapshot{

    private static final byte[] emptyData = new byte[2048];
    private static final byte[] emptySkyLight = new byte[2048];

    static {
        Arrays.fill(emptySkyLight, (byte)-1);
    }

    private final int x;
    private final int z;
    private final String worldname;
    private final short[][] blockids;
    private final byte[][] blockdata;
    private final byte[][] skylight;
    private final byte[][] emitlight;
    private final boolean[] empty;
    private final int[] hmap;
    private final long captureFulltime;
    private final BiomeBase[] biome;
    private final double[] biomeTemp;
    private final double[] biomeRain;

    private VoxelChunkSnapshot(int x, int z, String wname, long wtime, short[][] sectionBlockIDs, byte[][] sectionBlockData, byte[][] sectionSkyLights, byte[][] sectionEmitLights, boolean[] sectionEmpty, int[] hmap, BiomeBase[] biome, double[] biomeTemp, double[] biomeRain) {
        this.x = x;
        this.z = z;
        this.worldname = wname;
        this.captureFulltime = wtime;
        this.blockids = sectionBlockIDs;
        this.blockdata = sectionBlockData;
        this.skylight = sectionSkyLights;
        this.emitlight = sectionEmitLights;
        this.empty = sectionEmpty;
        this.hmap = hmap;
        this.biome = biome;
        this.biomeTemp = biomeTemp;
        this.biomeRain = biomeRain;
    }

    public int getX() {
        return this.x;
    }

    public int getZ() {
        return this.z;
    }

    public String getWorldName() {
        return this.worldname;
    }

    public final int getBlockTypeId(int x, int y, int z) {
        return this.blockids[y >> 4][(y & 15) << 8 | z << 4 | x];
    }

    public final int getBlockData(int x, int y, int z) {
        int off = (y & 15) << 7 | z << 3 | x >> 1;
        return this.blockdata[y >> 4][off] >> ((x & 1) << 2) & 15;
    }

    public final int getBlockSkyLight(int x, int y, int z) {
        int off = (y & 15) << 7 | z << 3 | x >> 1;
        return this.skylight[y >> 4][off] >> ((x & 1) << 2) & 15;
    }

    public final int getBlockEmittedLight(int x, int y, int z) {
        int off = (y & 15) << 7 | z << 3 | x >> 1;
        return this.emitlight[y >> 4][off] >> ((x & 1) << 2) & 15;
    }

    public final int getHighestBlockYAt(int x, int z) {
        return this.hmap[z << 4 | x];
    }

    public final Biome getBiome(int x, int z) {
        return CraftBlock.biomeBaseToBiome(this.biome[z << 4 | x]);
    }

    public final double getRawBiomeTemperature(int x, int z) {
        return this.biomeTemp[z << 4 | x];
    }

    public final double getRawBiomeRainfall(int x, int z) {
        return this.biomeRain[z << 4 | x];
    }

    public final long getCaptureFullTime() {
        return this.captureFulltime;
    }

    public final boolean isSectionEmpty(int sy) {
        return this.empty[sy];
    }

    public static VoxelChunkSnapshot fromChunk(org.bukkit.Chunk bukkitChunk){
        net.minecraft.server.v1_8_R3.Chunk chunk = ((CraftChunk)bukkitChunk).getHandle();
        ChunkSection[] cs = chunk.getSections();

        short[][] sectionBlockIDs = new short[cs.length][];
        byte[][] sectionBlockData = new byte[cs.length][];
        byte[][] sectionSkyLights = new byte[cs.length][];
        byte[][] sectionEmitLights = new byte[cs.length][];
        boolean[] sectionEmpty = new boolean[cs.length];

        int i;
        for(int hmap = 0; hmap < cs.length; ++hmap) {
            if(cs[hmap] == null) {
                sectionBlockIDs[hmap] = new short[4096];
                //sectionBlockIDs[hmap] = emptyBlockIds;    Is modified, results in duplicated chunk parts
                sectionBlockData[hmap] = emptyData;
                sectionSkyLights[hmap] = emptySkyLight;
                sectionEmitLights[hmap] = emptyData;
                sectionEmpty[hmap] = true;
            } else {
                short[] biome = new short[4096];
                char[] idArray = cs[hmap].getIdArray();
                byte[] biomeRain = sectionBlockData[hmap] = new byte[2048];

                for(int world = 0; world < 4096; ++world) {
                    if(idArray[world] != 0) {
                        IBlockData dat = net.minecraft.server.v1_8_R3.Block.d.a(idArray[world]);
                        if(dat != null) {
                            biome[world] = (short)net.minecraft.server.v1_8_R3.Block.getId(dat.getBlock());
                            i = dat.getBlock().toLegacyData(dat);
                            int jj = world >> 1;
                            if((world & 1) == 0) {
                                biomeRain[jj] = (byte)(biomeRain[jj] & 240 | i & 15);
                            } else {
                                biomeRain[jj] = (byte)(biomeRain[jj] & 15 | (i & 15) << 4);
                            }
                        }
                    }
                }

                sectionBlockIDs[hmap] = biome;
                if(cs[hmap].getSkyLightArray() == null) {
                    sectionSkyLights[hmap] = emptyData;
                } else {
                    sectionSkyLights[hmap] = new byte[2048];
                    System.arraycopy(cs[hmap].getSkyLightArray().a(), 0, sectionSkyLights[hmap], 0, 2048);
                }

                sectionEmitLights[hmap] = new byte[2048];
                System.arraycopy(cs[hmap].getEmittedLightArray().a(), 0, sectionEmitLights[hmap], 0, 2048);
            }
        }

        int[] var19 = new int[256];
        System.arraycopy(chunk.heightMap, 0, var19, 0, 256);

        org.bukkit.World var24 = bukkitChunk.getWorld();

        return new VoxelChunkSnapshot(bukkitChunk.getX(), bukkitChunk.getZ(), var24.getName(), var24.getFullTime(), sectionBlockIDs, sectionBlockData, sectionSkyLights, sectionEmitLights, sectionEmpty, var19, null, null, null);
    }

}

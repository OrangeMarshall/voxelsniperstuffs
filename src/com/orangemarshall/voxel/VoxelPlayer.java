package com.orangemarshall.voxel;

import com.google.common.collect.Maps;
import com.orangemarshall.voxel.task.AsyncTask;
import com.orangemarshall.voxel.util.BlockType;
import com.orangemarshall.voxel.util.HistoryList;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;

import java.util.Collection;
import java.util.Map;

import static org.bukkit.ChatColor.*;


public class VoxelPlayer{

    private static Map<Player, VoxelPlayer> players = Maps.newHashMap();

    private HistoryList<AsyncTask> revertTasks = new HistoryList<>(20);

    public final Player player;

    private Voxel.Tool selectedTool = Voxel.Tool.REPLACE_CUBE;
    private BlockType selectedBlockType = new BlockType(Material.STONE, (byte)0);
    private int selectedToolSize = 3;

    public static void registerOnlinePlayers(){
        Bukkit.getOnlinePlayers().forEach(VoxelPlayer::getOrCreatePlayer);
    }

    private VoxelPlayer(Player player){
        this.player = player;
    }

    public BlockType getSelectedType(){
        return selectedBlockType;
    }

    public int getSelectedToolSize(){
        return selectedToolSize;
    }

    public Voxel.Tool getSelectedTool(){
        return selectedTool;
    }

    public void setSelectedToolSize(int toolSize){
        this.selectedToolSize = toolSize;
    }

    public void setSelectedTool(Voxel.Tool tool){
        this.selectedTool = tool;
    }

    public void setSelectedType(Material material, byte data){
        this.selectedBlockType = new BlockType(material, data);
    }

    public HistoryList<AsyncTask> getHistoryList(){
        return revertTasks;
    }

    public String getActionbarInfo(){
        return WHITE.toString() + BOLD + "Tool: " + GOLD + selectedTool.getName()
                + WHITE + " - "
                + WHITE.toString() + BOLD + "Size: " + GRAY + selectedToolSize
                + WHITE + " - "
                + WHITE.toString() + BOLD + "Material: " + GREEN + selectedBlockType.material
                + WHITE + " (" + AQUA + selectedBlockType.data + WHITE + ")";
    }

    public static Collection<VoxelPlayer> getPlayers(){
        return players.values();
    }

    public static VoxelPlayer removePlayer(Player player){
        return players.remove(player);
    }

    public static VoxelPlayer getOrCreatePlayer(Player player) {
        VoxelPlayer voxelPlayer = players.get(player);

        if(voxelPlayer == null){
            voxelPlayer = new VoxelPlayer(player);
            players.put(player, voxelPlayer);
        }

        return voxelPlayer;
    }

}

package com.orangemarshall.voxel.gui;

import com.orangemarshall.voxel.util.ItemStacks;
import com.orangemarshall.voxel.util.gui.GuiHolder;
import com.orangemarshall.voxel.util.gui.LayeredGuiScreen;
import com.orangemarshall.voxel.util.gui.element.GuiElement;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.List;


public abstract class PagedGuiScreen extends LayeredGuiScreen{

    private static final int itemsPerPage = 15;

    private final List<GuiElement> elements;

    protected final int totalPages;

    protected int currentPage = 1;

    public PagedGuiScreen(GuiHolder guiHolder, List<GuiElement> elements){
        super(guiHolder);
        this.totalPages = (elements.size() / itemsPerPage) + 1;
        this.elements = elements;
    }

    @Override
    public void drawScreen(Inventory inventory){
        clearElements();
        loadContents();

        super.drawScreen(inventory);
    }

    @Override
    public String getTitle(){
        return "Page " + currentPage;
    }

    protected void loadContents(){
        loadNavigators();
        loadElements();
    }

    private void loadElements(){
        int startIndex = (currentPage-1) * itemsPerPage;
        int endIndex = Math.min(elements.size(), startIndex + itemsPerPage);

        int rowOffset = 2;
        int columnOffset = 2;

        int counter = 0;

        while(startIndex < endIndex){
            int itemsPerRow = 5;
            int row = rowOffset + counter / itemsPerRow;
            int column = columnOffset + counter % itemsPerRow;

            addElement(elements.get(startIndex), row, column);

            startIndex++;
            counter++;
        }
    }

    private void loadNavigators(){
        if(currentPage > 1){
            addElement(new Navigator(false), 4, 1);
        }

        if(currentPage < totalPages){
            addElement(new Navigator(true), 4, 7);
        }
    }

    private class Navigator implements GuiElement{

        private boolean forward;

        private Navigator(boolean forward){
            this.forward = forward;
        }

        @Override
        public void onClick(){
            if(forward){
                PagedGuiScreen.this.currentPage++;
            }else{
                PagedGuiScreen.this.currentPage--;
            }

            Player player = guiHolder.getPlayer();
            player.playSound(player.getLocation(), Sound.WOOD_CLICK, 1.0f, 1.0f);

            guiHolder.updateScreen();
        }

        @Override
        public ItemStack getItemStack(){
            if(forward){
                String name = ChatColor.WHITE + "Next page";
                return ItemStacks.Builder.create(Material.ARROW).setName(name).build();
            }else{
                String name = ChatColor.WHITE + "Previous Page";
                return ItemStacks.Builder.create(Material.ARROW).setName(name).build();
            }
        }

    }
}

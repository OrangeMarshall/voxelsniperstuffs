package com.orangemarshall.voxel.gui;

import com.orangemarshall.voxel.Voxel;
import com.orangemarshall.voxel.VoxelPlayer;
import com.orangemarshall.voxel.util.BlockType;
import com.orangemarshall.voxel.util.ItemStacks;
import com.orangemarshall.voxel.util.gui.GuiHolder;
import com.orangemarshall.voxel.util.gui.LayeredGuiScreen;
import org.apache.commons.lang.WordUtils;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;


public class VoxelMainMenu extends LayeredGuiScreen{

    private Player player;

    public VoxelMainMenu(GuiHolder guiHolder, Player player){
        super(guiHolder);
        this.player = player;

        loadElements();
    }

    private void loadElements(){
        InfoElement top = new InfoElement(Material.BEACON, "VoxelSniper Help", new String[]{
                "This GUI will help you get started with",
                "the basics of this plugin!"
        });

        InfoElement toolHelp = new InfoElement(Material.SIGN, "Voxel Tools", new String[]{
                "Tools will help you modify the",
                "world faster. You can change it",
                "by doing /vtool <name>. Or you",
                "can click below to choose one! ",
                "Do /vundo in order to revert ",
                "your last action. Hold a brown",
                "mushroom and right click on a",
                "block to use a tool.",
                "Example: /vtool ball"
        });

        InfoElement sizeHelp = new InfoElement(Material.SIGN, "Tool Size", new String[]{
                "Some tools can be adjusted",
                "by setting the size.",
                "Example: /vsize 3"
        });

        InfoElement materialHelp = new InfoElement(Material.SIGN, "Material", new String[]{
                "You can set your currently",
                "selected material by ",
                "doing /vmaterial <material>",
                "or /vmaterial <material> <data>.",
                "Example: /material stone 2"
        });

        VoxelPlayer voxelPlayer = VoxelPlayer.getOrCreatePlayer(player);
        Voxel.Tool tool = voxelPlayer.getSelectedTool();
        int size = voxelPlayer.getSelectedToolSize();
        BlockType type = voxelPlayer.getSelectedType();

        ClickableInfoElement openSelectTool = new ClickableInfoElement(guiHolder, new ToolSelectorScreen(guiHolder),
                Material.BROWN_MUSHROOM, tool.getName(), new String[]{"The currently selected tool."});

        InfoElement infoSelectSize = new InfoElement(Material.TORCH, "Size: " + size,
                new String[]{"The currently selected size."}, size);

        String nameMaterial = ChatColor.YELLOW + WordUtils.capitalizeFully(type.material.name() + " (" + type.data + ")");
        String[] loreMaterial = new String[]{ChatColor.GRAY + "The currently selected material."};
        ItemStack itemMaterial = ItemStacks.Builder.create(type.material).setData(type.data).setName(nameMaterial).setLore(loreMaterial).build();
        InfoElement infoSelectMaterial = new InfoElement(itemMaterial);

        addElement(top, 1, 4);
        addElement(toolHelp, 3, 2);
        addElement(sizeHelp, 3, 4);
        addElement(materialHelp, 3, 6);

        addElement(openSelectTool, 4, 2);
        addElement(infoSelectSize, 4, 4);
        addElement(infoSelectMaterial, 4, 6);
    }

    @Override
    public String getTitle(){
        return "VoxelSniper Menu";
    }
}

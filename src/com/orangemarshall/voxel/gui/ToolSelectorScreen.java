package com.orangemarshall.voxel.gui;

import com.orangemarshall.voxel.Voxel;
import com.orangemarshall.voxel.VoxelPlayer;
import com.orangemarshall.voxel.util.ItemStacks;
import com.orangemarshall.voxel.util.gui.GuiHolder;
import com.orangemarshall.voxel.util.gui.element.GuiElement;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.stream.Collectors;
import java.util.stream.Stream;


public class ToolSelectorScreen extends PagedGuiScreen{

    public ToolSelectorScreen(GuiHolder guiHolder){
        super(guiHolder, Stream.of(Voxel.Tool.values()).map(tool -> new ToolElement(tool, guiHolder)).collect(Collectors.toList()));
    }

    @Override
    public String getTitle(){
        return "Tools - " + super.getTitle();
    }

    private static class ToolElement implements GuiElement{

        private Voxel.Tool tool;
        private GuiHolder guiHolder;

        public ToolElement(Voxel.Tool tool, GuiHolder guiHolder){
            this.tool = tool;
            this.guiHolder = guiHolder;
        }

        @Override
        public void onClick(){
            Player player = guiHolder.getPlayer();
            VoxelPlayer.getOrCreatePlayer(player).setSelectedTool(tool);
            player.playSound(player.getLocation(), Sound.CLICK, 1.0f, 1.0f);

            guiHolder.updateScreen();
        }

        @Override
        public ItemStack getItemStack(){
            byte data;

            if(isSelected()){
                data = (byte) 10;
            }else{
                data = (byte) 8;
            }

            String name = ChatColor.WHITE + tool.getName();
            return ItemStacks.Builder.create(Material.INK_SACK).setName(name).setData(data).build();
        }

        private boolean isSelected(){
            VoxelPlayer voxelPlayer = VoxelPlayer.getOrCreatePlayer(guiHolder.getPlayer());
            return voxelPlayer.getSelectedTool() == this.tool;
        }
    }

}

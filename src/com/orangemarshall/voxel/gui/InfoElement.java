package com.orangemarshall.voxel.gui;

import com.orangemarshall.voxel.util.ItemStacks;
import com.orangemarshall.voxel.util.gui.element.GuiElement;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;


public class InfoElement implements GuiElement{

    private ItemStack itemStack;

    private String namePrefix = ChatColor.YELLOW.toString();
    private String lorePrefix = ChatColor.GRAY.toString();

    public InfoElement(Material material, String name, String[] lore, int amount){
        for(int i = 0; i < lore.length; i++){
            lore[i] = lorePrefix + lore[i];
        }
        name = namePrefix + name;

        itemStack = ItemStacks.Builder.create(material)
                .setName(name)
                .setLore(lore)
                .setAmount(amount)
                .build();
    }

    public InfoElement(ItemStack itemStack){
        this.itemStack = itemStack;
    }

    public InfoElement(Material material, String name, String[] lore){
        this(material, name, lore, 1);
    }

    @Override
    public void onClick(){}

    @Override
    public ItemStack getItemStack(){
        return itemStack;
    }

}

package com.orangemarshall.voxel.gui;

import com.orangemarshall.voxel.util.gui.GuiHolder;
import com.orangemarshall.voxel.util.gui.GuiScreen;
import org.bukkit.Material;


public class ClickableInfoElement extends InfoElement{

    private GuiScreen screen;
    private GuiHolder holder;

    public ClickableInfoElement(GuiHolder holder, GuiScreen guiScreen, Material material, String name, String[] lore){
        super(material, name, lore);
        this.screen = guiScreen;
        this.holder = holder;
    }

    @Override
    public void onClick(){
        holder.openChild(screen);
    }
}

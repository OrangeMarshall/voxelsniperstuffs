package com.orangemarshall.voxel.util;

import org.bukkit.Material;


public class BlockType{

    public final Material material;
    public final byte data;
    public final short combinedId;

    public BlockType(Material material, byte data){
        this.material = material;
        this.data = data;
        this.combinedId = Util.getCombinedId(material, data);
    }

    public BlockType(int materialId, byte data){
        this.material = Material.getMaterial(materialId);
        this.data = data;
        this.combinedId = Util.getCombinedId(material, data);
    }

    @Override
    public String toString(){
        return String.format("BlockType[material=%s,data=%d,combinedId=%d]", material.toString(), data, combinedId);
    }

}

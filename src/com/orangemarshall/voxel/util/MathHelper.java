package com.orangemarshall.voxel.util;

public class MathHelper{

    public static int pow(int base, int exp){
        int result = 1;

        while(exp > 0){
            if((exp & 1) > 0){
                result *= base;
            }
            exp >>= 1;
            base *= base;
        }

        return result;
    }

}

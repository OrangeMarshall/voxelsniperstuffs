package com.orangemarshall.voxel.util;

import com.google.common.collect.Lists;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;


public class ItemStacks{

    public static class Builder{

        private Material material;
        private byte data = (byte) 0;
        private String name = "";
        private String[] lore = new String[0];
        private int amount = 1;
        private short damage = (short) 0;

        private Builder(Material material){
            this.material = material;
        }

        public static Builder create(Material material){
            return new Builder(material);
        }

        public Builder setType(Material material){
            this.material = material;
            return this;
        }

        public Builder setData(byte data){
            this.data = data;
            return this;
        }

        public Builder setName(String name){
            this.name = name;
            return this;
        }

        public Builder setLore(String[] lore){
            this.lore = lore;
            return this;
        }

        public Builder setAmount(int amount){
            this.amount = amount;
            return this;
        }

        public Builder setDamage(short damage){
            this.damage = damage;
            return this;
        }

        public ItemStack build(){
            ItemStack item = new ItemStack(material, amount, damage, data);
            ItemStacks.setName(item, name, lore);
            return item;
        }

    }

    public static void setName(ItemStack itemStack, String name, String[] lore){
        ItemMeta meta = itemStack.getItemMeta();
        meta.setDisplayName(name);
        meta.setLore(Lists.newArrayList(lore));
        itemStack.setItemMeta(meta);
    }

}

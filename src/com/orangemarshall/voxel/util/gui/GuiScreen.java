package com.orangemarshall.voxel.util.gui;

import com.orangemarshall.voxel.util.gui.element.GuiElement;
import org.bukkit.inventory.Inventory;

import java.util.Optional;


public abstract class GuiScreen{

    protected Optional<GuiScreen> parent;
    protected GuiHolder guiHolder;

    protected int rows, columns;

    private GuiElement[][] elements;

    public GuiScreen(GuiHolder guiHolder){
        this.parent = Optional.empty();
        this.guiHolder = guiHolder;

        this.rows = guiHolder.getRows();
        this.columns = guiHolder.getColumns();

        elements = new GuiElement[rows][columns];
    }

    public void drawScreen(Inventory inventory){
        for(int row = 0; row < rows; row++){
            for(int column = 0; column < columns; column++){
                drawSlot(inventory, row, column);
            }
        }
    }

    public void addElement(GuiElement element, int row, int column){
        elements[row][column] = element;
    }

    public void clearElements(){
        elements = new GuiElement[rows][columns];
    }

    protected void drawSlot(Inventory inventory, int row, int column){
        GuiElement element = elements[row][column];

        if(element != null){
            inventory.setItem(row * 9 + column, element.getItemStack());
        }
    }

    public Optional<GuiScreen> getParent(){
        return parent;
    }

    public void setParent(GuiScreen guiScreen){
        this.parent = Optional.ofNullable(guiScreen);
    }

    public boolean hasParent(){
        return parent.isPresent();
    }

    // Called before #drawScreen
    public void onOpen(){}

    public void onClose(){}

    public void onMouseClick(int row, int column){
        GuiElement element = elements[row][column];

        if(element != null){
            element.onClick();
        }
    }

    public abstract String getTitle();

}

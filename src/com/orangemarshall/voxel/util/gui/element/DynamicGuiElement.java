package com.orangemarshall.voxel.util.gui.element;

import org.bukkit.inventory.ItemStack;


public interface DynamicGuiElement extends GuiElement{

    void setItemStack(ItemStack itemStack);

}

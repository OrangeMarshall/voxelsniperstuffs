package com.orangemarshall.voxel.util.gui.element;

import com.orangemarshall.voxel.util.ItemStacks;
import com.orangemarshall.voxel.util.gui.GuiHolder;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;


public class Close implements GuiElement{

    private GuiHolder holder;

    public Close(GuiHolder holder){
        this.holder = holder;
    }

    @Override
    public void onClick(){
        holder.closeGuiAndInventory();
    }

    @Override
    public ItemStack getItemStack(){
        ItemStack item = new ItemStack(Material.BARRIER);

        String name = ChatColor.RED + "Close";
        ItemStacks.setName(item, name, new String[0]);

        return item;
    }
}

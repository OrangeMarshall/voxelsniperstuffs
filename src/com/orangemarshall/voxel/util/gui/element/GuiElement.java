package com.orangemarshall.voxel.util.gui.element;

import org.bukkit.inventory.ItemStack;


public interface GuiElement{

    void onClick();
    ItemStack getItemStack();

}

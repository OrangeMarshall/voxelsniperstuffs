package com.orangemarshall.voxel.util.gui.element;

import com.orangemarshall.voxel.util.ItemStacks;
import com.orangemarshall.voxel.util.gui.GuiHolder;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;


public class Back implements GuiElement{

    private GuiHolder holder;

    public Back(GuiHolder holder){
        this.holder = holder;
    }

    @Override
    public void onClick(){
        holder.closeChild();
    }

    @Override
    public ItemStack getItemStack(){
        ItemStack itemStack = new ItemStack(Material.ARROW);
        ItemStacks.setName(itemStack, ChatColor.GREEN + "Go Back", new String[0]);
        return itemStack;
    }
}

package com.orangemarshall.voxel.util.gui.element;

import com.orangemarshall.voxel.util.gui.GuiHolder;
import com.orangemarshall.voxel.util.gui.GuiScreen;
import org.bukkit.inventory.ItemStack;


public class OpenScreen implements GuiElement{

    private GuiHolder holder;
    private GuiScreen screen;
    private ItemStack itemStack;

    public OpenScreen(GuiHolder holder, GuiScreen screen, ItemStack itemStack){
        this.holder = holder;
        this.screen = screen;
        this.itemStack = itemStack;
    }

    @Override
    public void onClick(){
        holder.openChild(screen);
    }

    @Override
    public ItemStack getItemStack(){
        return itemStack;
    }

}

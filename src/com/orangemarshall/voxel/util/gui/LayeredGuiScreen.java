package com.orangemarshall.voxel.util.gui;

import com.orangemarshall.voxel.util.gui.element.Back;
import com.orangemarshall.voxel.util.gui.element.Close;
import org.bukkit.inventory.Inventory;


public abstract class LayeredGuiScreen extends GuiScreen{

    public LayeredGuiScreen(GuiHolder guiHolder){
        super(guiHolder);
    }

    @Override
    public void drawScreen(Inventory inventory){
        if(hasParent()){
            addElement(new Back(guiHolder), 6, 4);
        }else{
            addElement(new Close(guiHolder), 6, 4);
        }

        super.drawScreen(inventory);
    }
}

package com.orangemarshall.voxel.util.gui;

import com.orangemarshall.voxel.util.Util;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.Optional;


public class GuiHolder{

    private Player player;
    private GuiScreen guiScreen;

    private Inventory inventory;

    private GuiListener guiListener;
    private JavaPlugin plugin;

    private String title;

    private int slots = 63; // Multiple of 9
    private int rows = slots / 9;
    private int columns = 9;


    public GuiHolder(JavaPlugin plugin){
        guiListener = new GuiListener(plugin, this);
        guiListener.register();
    }

    public void setTitle(String title){
        this.title = title;
    }

    public void openGui(GuiScreen screen, Player player){
        this.player = player;
        this.guiScreen = screen;
        this.guiScreen.onOpen();

        inventory = Bukkit.createInventory(player, slots);
        player.openInventory(inventory);

        updateScreen();
    }

    public void closeGui(){
        guiScreen.onClose();
        guiListener.unregister();
    }

    public void closeGuiAndInventory(){
        closeGui();
        player.closeInventory();
    }

    public void closeChild(){
        Optional<GuiScreen> parent = guiScreen.getParent();

        if(parent.isPresent()){
            guiScreen.onClose();
            guiScreen = parent.get();
            guiScreen.onOpen();

            updateScreen();
        }else{
            closeGuiAndInventory();
        }
    }

    public int getRows(){
        return rows;
    }

    public int getColumns(){
        return columns;
    }

    // Cancel the click?
    public boolean onSlotClick(int slot){
        int row = slot / 9;
        int column = slot % 9;
        guiScreen.onMouseClick(row, column);

        return true;
    }

    public Inventory getInventory(){
        return inventory;
    }

    public Player getPlayer(){
        return player;
    }

    public void openChild(GuiScreen guiScreen){
        guiScreen.setParent(this.guiScreen);
        this.guiScreen = guiScreen;
        this.guiScreen.onOpen();

        updateScreen();
    }

    public void updateScreen(){
        inventory.clear();
        Util.updateInventory(player, guiScreen.getTitle());
        guiScreen.drawScreen(inventory);
    }

}

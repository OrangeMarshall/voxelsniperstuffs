package com.orangemarshall.voxel.util.gui;

import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.plugin.java.JavaPlugin;


public class GuiListener implements Listener{

    private JavaPlugin plugin;
    private GuiHolder guiHolder;

    public GuiListener(JavaPlugin plugin, GuiHolder guiHolder){
        this.plugin = plugin;
        this.guiHolder = guiHolder;
    }

    @EventHandler(ignoreCancelled = true)
    public void onClick(InventoryClickEvent event){
        boolean cancelled = guiHolder.onSlotClick(event.getSlot());
        event.setCancelled(cancelled);
    }

    @EventHandler
    public void onClose(InventoryCloseEvent event){
        if(event.getInventory().getHolder() == guiHolder.getInventory().getHolder()){
            guiHolder.closeGui();
        }
    }

    public void register(){
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }

    public void unregister(){
        HandlerList.unregisterAll(this);
    }


}

package com.orangemarshall.voxel.util;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Sets;
import com.google.common.collect.Table;
import com.orangemarshall.voxel.VoxelChunkSnapshot;
import net.minecraft.server.v1_8_R3.*;
import org.bukkit.Chunk;
import org.bukkit.craftbukkit.v1_8_R3.CraftChunk;
import org.bukkit.craftbukkit.v1_8_R3.CraftWorld;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;


public class WorldSnapshot {

    private static final FieldWrapper<short[][]> blockids = new FieldWrapper<>("blockids", VoxelChunkSnapshot.class);
    private static final FieldWrapper<byte[][]> blockdata = new FieldWrapper<>("blockdata", VoxelChunkSnapshot.class);

    private static final FieldWrapper<Map<BlockPosition, TileEntity>> tileEntites = new FieldWrapper<>("tileEntities", net.minecraft.server.v1_8_R3.Chunk.class);
    private static final FieldWrapper<List<Entity>[]> entitySlices = new FieldWrapper<>("entitySlices", net.minecraft.server.v1_8_R3.Chunk.class);

    private Table<Integer, Integer, ChunkEntry> chunks = HashBasedTable.create();

    private final World nms_world;

    // Creates a partial world perception with thread-safe methods to modify blocks (based on chunksnapshots)
    // using absolute (not chunk-relative) coords
    public WorldSnapshot(org.bukkit.World world, Collection<Chunk> chunks){
        this.nms_world = ((CraftWorld) world).getHandle();

        chunks.forEach(chunk -> {
            int x = chunk.getX();
            int z = chunk.getZ();
            this.chunks.put(x, z, ChunkEntry.fromChunk(chunk));
        });
    }

    public BlockType getTypeAt(int x, int y, int z){
        int chunkX = x >> 4;
        int chunkZ = z >> 4;
        VoxelChunkSnapshot snapshot = chunks.get(chunkX, chunkZ).snapshot;

        return new BlockType(snapshot.getBlockTypeId(x&15, y, z&15), (byte) snapshot.getBlockData(x&15, y, z&15));
    }

    public void setTypeAt(int x, int y, int z, BlockType type){
        int chunkX = x >> 4;
        int chunkZ = z >> 4;
        VoxelChunkSnapshot snapshot = chunks.get(chunkX, chunkZ).snapshot;

        blockids.get(snapshot)[y >> 4][(y&15) << 8 | (z&15) << 4 | (x&15)] = (short) Block.getId(Block.getByCombinedId(type.combinedId).getBlock());

        int off = (y&15) << 7 | (z&15) << 3 | (x&15) >> 1;
        byte[] dataArr = blockdata.get(snapshot)[y >> 4];

        dataArr[off] = (byte)(dataArr[off] & 240 | type.data & 15);
        dataArr[off] = (byte)(dataArr[off] & 15 | (type.data & 15) << 4);
    }

    // Creates chunks based on the current world perception
    public Set<net.minecraft.server.v1_8_R3.Chunk> createChunks(){
        Set<net.minecraft.server.v1_8_R3.Chunk> chunks = Sets.newHashSet();

        for(Table.Cell<Integer, Integer, ChunkEntry> cell : this.chunks.cellSet()){
            ChunkEntry chunk = cell.getValue();

            net.minecraft.server.v1_8_R3.ChunkSnapshot nms_snapshot = ChunkUtil.createNmsChunkSnapshot(chunk.snapshot);
            net.minecraft.server.v1_8_R3.Chunk nms_chunk = new net.minecraft.server.v1_8_R3.Chunk(nms_world, nms_snapshot, cell.getRowKey(), cell.getColumnKey());
            nms_chunk.bukkitChunk = new CraftChunk(nms_chunk);

            entitySlices.setFinal(nms_chunk, chunk.entitySlices);
            tileEntites.setFinal(nms_chunk, chunk.tileEntities);

            chunks.add(nms_chunk);
        }

        return chunks;
    }


    private static class ChunkEntry{

        public final VoxelChunkSnapshot snapshot;
        private final List<Entity>[] entitySlices;
        private final Map<BlockPosition, TileEntity> tileEntities;

        private ChunkEntry(VoxelChunkSnapshot snapshot, List<Entity>[] entitySlices, Map<BlockPosition, TileEntity> tileEntities){
            this.snapshot = snapshot;
            this.entitySlices = entitySlices;
            this.tileEntities = tileEntities;
        }

        public static ChunkEntry fromChunk(Chunk chunk){
            VoxelChunkSnapshot snapshot = ChunkUtil.getChunkSnapshot(chunk);
            net.minecraft.server.v1_8_R3.Chunk nms_chunk = ((CraftChunk) chunk).getHandle();

            List<Entity>[] entitySlices = WorldSnapshot.entitySlices.get(nms_chunk);
            Map<BlockPosition, TileEntity> tileEntities = nms_chunk.tileEntities;

            return new ChunkEntry(snapshot, entitySlices, tileEntities);
        }

    }
}

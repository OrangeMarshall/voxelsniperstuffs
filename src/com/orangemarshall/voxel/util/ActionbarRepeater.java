package com.orangemarshall.voxel.util;

import com.orangemarshall.voxel.Voxel;
import com.orangemarshall.voxel.VoxelPlayer;
import org.bukkit.scheduler.BukkitScheduler;


public class ActionbarRepeater{

    private Voxel plugin;

    private int taskId = -1;
    private int periodInTicks = 10;

    public ActionbarRepeater(Voxel plugin){
        this.plugin = plugin;
    }

    public void setPeriod(int ticks){
        this.periodInTicks = ticks;
    }

    public void restart(){
        BukkitScheduler scheduler = plugin.getServer().getScheduler();

        if(taskId >= 0 && scheduler.isCurrentlyRunning(taskId)){
            scheduler.cancelTask(taskId);
        }

        taskId = scheduler.scheduleSyncRepeatingTask(plugin, new RepeaterTask(), 0, periodInTicks);
    }

    private static class RepeaterTask implements Runnable{

        @Override
        public void run(){
            VoxelPlayer.getPlayers().forEach(this::sendInfoToPlayer);
        }

        private void sendInfoToPlayer(VoxelPlayer voxelPlayer){
            String message = voxelPlayer.getActionbarInfo();
            Util.sendActionbarMessage(voxelPlayer.player, message);
        }

    }

}

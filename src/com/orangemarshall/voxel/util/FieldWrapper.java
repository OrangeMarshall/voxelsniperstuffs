package com.orangemarshall.voxel.util;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;


public class FieldWrapper<T> {

    private static Field modifiersField;
    private Field field;

    static{
        try {
            modifiersField = Field.class.getDeclaredField("modifiers");
            modifiersField.setAccessible(true);
        } catch (NoSuchFieldException | SecurityException e) {
            e.printStackTrace();
        }
    }

    public FieldWrapper(String fieldName, Class<?> clazz){
        try {
            field = clazz.getDeclaredField(fieldName);
            field.setAccessible(true);
        } catch (NoSuchFieldException | SecurityException e) {
            e.printStackTrace();
        }
    }

    @SuppressWarnings("unchecked")
    public T get(Object obj){
        try {
            return (T) field.get(obj);
        } catch (IllegalArgumentException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void set(Object obj, T value){
        try {
            field.set(obj, value);
        } catch (IllegalArgumentException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    public void setFinal(Object obj, T value){
        try {
            modifiersField.setInt(field, field.getModifiers() & ~Modifier.FINAL);
            field.set(obj, value);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}

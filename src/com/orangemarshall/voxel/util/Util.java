package com.orangemarshall.voxel.util;

import net.minecraft.server.v1_8_R3.*;
import org.bukkit.*;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.util.BlockIterator;
import org.bukkit.util.Vector;


public class Util{

    // Ignores air. Returns null if no selectedBlock in range.
    public static Block getBlockLookedAt(Player player){
        World world = player.getWorld();
        Vector start = player.getEyeLocation().toVector();
        Vector direction = player.getEyeLocation().getDirection();
        int maxDistance = 140;

        BlockIterator blockIterator = new BlockIterator(world, start, direction, 0, maxDistance);

        while(blockIterator.hasNext()){
            Block block = blockIterator.next();

            if(block.getType() != Material.AIR){
                return block;
            }
        }

        return null;
    }

    public static void sendInfoMessage(Player player, String message){
        sendMessage(player, ChatColor.YELLOW, message);
    }

    public static void sendErrorMessage(Player player, String message){
        sendMessage(player, ChatColor.RED, message);
    }

    private static void sendMessage(Player player, ChatColor color, String message){
        String prefix = ChatColor.GRAY + "[" + ChatColor.GOLD + "Voxel" + ChatColor.GRAY + "] ";
        player.sendMessage(prefix + color + message);
    }

    public static short getCombinedId(Material material, byte data){
        return (short) (material.getId() + (data << 12));
    }

    public static void sendPacketToAll(Packet packet){
        Bukkit.getOnlinePlayers().forEach(player -> sendPacketToPlayer(player, packet));
    }

    public static void sendPacketToPlayer(Player player, Packet packet){
        ((CraftPlayer)player).getHandle().playerConnection.sendPacket(packet);
    }

    public static void sendActionbarMessage(Player player, String message){
        IChatBaseComponent component = IChatBaseComponent.ChatSerializer.a("{text:\"" + message + "\"}");
        sendPacketToPlayer(player, new PacketPlayOutChat(component, (byte) 2));
    }

    public static void updateInventory(Player player, String title){
        EntityPlayer entityPlayer = ((CraftPlayer) player).getHandle();

        int windowId = entityPlayer.activeContainer.windowId;
        ChatMessage titleComp = new ChatMessage(title);
        int size = player.getOpenInventory().getTopInventory().getSize();

        PacketPlayOutOpenWindow packet = new PacketPlayOutOpenWindow(windowId, "", titleComp, size);

        entityPlayer.playerConnection.sendPacket(packet);
        entityPlayer.updateInventory(entityPlayer.activeContainer);
    }

}

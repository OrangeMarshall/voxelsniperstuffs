package com.orangemarshall.voxel.util;

import com.orangemarshall.voxel.VoxelChunkSnapshot;
import net.minecraft.server.v1_8_R3.ChunkSnapshot;
import net.minecraft.server.v1_8_R3.IBlockData;
import org.bukkit.Material;


public class ChunkUtil{


    public static ChunkSnapshot createNmsChunkSnapshot(org.bukkit.ChunkSnapshot snapshot){
        net.minecraft.server.v1_8_R3.ChunkSnapshot nms_snapshot = new net.minecraft.server.v1_8_R3.ChunkSnapshot();

        for(int x = 0; x < 16; x++){
            for(int y = 0; y < 256; y++){
                for(int z = 0; z < 16; z++){
                    int typeId = snapshot.getBlockTypeId(x, y, z);
                    byte data = (byte) snapshot.getBlockData(x, y, z);
                    int combinedId = Util.getCombinedId(Material.getMaterial(typeId), data);

                    IBlockData iBlockData = net.minecraft.server.v1_8_R3.Block.getByCombinedId(combinedId);

                    nms_snapshot.a(x, y, z, iBlockData);
                }
            }
        }

        return nms_snapshot;
    }

    public static VoxelChunkSnapshot getChunkSnapshot(org.bukkit.Chunk bukkitChunk){
        return VoxelChunkSnapshot.fromChunk(bukkitChunk);
    }
}

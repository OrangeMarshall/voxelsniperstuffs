package com.orangemarshall.voxel.util;

import com.google.common.collect.Lists;

import java.util.List;


public class HistoryList<T> {

    private List<T> entries = Lists.newLinkedList();
    private int maxSize;

    public HistoryList(int maxSize){
        this.maxSize = maxSize;
    }

    public void add(T entry){
        entries.add(0, entry);

        if(entries.size() > maxSize)
            entries.remove(entries.size() - 1);
    }

    public T getMostRecent(){
        return entries.size() > 0 ? entries.get(0) : null;
    }

    public void removeMostRecent(){
        if(entries.size() > 0 ){
            entries.remove(0);
        }
    }

}
